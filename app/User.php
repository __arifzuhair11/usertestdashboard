<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Str;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'date_added', 'company', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public static function applyFilters(Request $filters)
    {
      $query = static::applyDecoratorsFromRequest($filters, (new User())->newQuery());
      return static::getResults($query);
    }

    private static function applyDecoratorsFromRequest(Request $request, Builder $query)
   {
       foreach ($request->all() as $filterName => $value) {

           $decorator = static::createFilterDecorator($filterName);
           if (static::isValidDecorator($decorator) && $value != '') {
               $query = $decorator::apply($query, $value);
           }

       }
       return $query;
   }

   private static function createFilterDecorator($name)
   {
     return __NAMESPACE__ . '\\UserSearch\\' . Str::studly($name);
   }

    private static function isValidDecorator($decorator)
   {
       return class_exists($decorator);
   }

   private static function getResults(Builder $query)
   {
       return $query->get();
   }
}
