<?php

namespace App\Http\Controllers;

use App\User;
use Session;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    public function filter(Request $request)
    {
      $users = User::applyFilters($request);
      $input = $request->only('first_name', 'last_name', 'email', 'company', 'hired_from', 'hired_to');
      Session::push('input', $input);
      if($users->count() > 0){
        Session::flash('success', 'Filters applied! ' . $users->count() . ' results found');
      }
      else {
        Session::flash('warning', 'Filters applied! ' . $users->count() . ' results found');
      }
      return view('search', compact('users', 'input'));

    }
}
