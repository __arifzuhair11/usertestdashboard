@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header">Filter Users</div>
          <div class="card-body">
            <form class="" action="{{route('search.filter')}}"method="post">
              @csrf
              <div class="form-row">
                <div class="col form-group">
                  <label for="first_name">First Name</label>
                  <input type="text" class="form-control" name="first_name" id="first_name" value="{{ Session::has('input') ? $input['first_name'] : '' }}" placeholder="eg: James">
                </div>
                <div class="col form-group">
                  <label for="last_name">Last Name</label>
                  <input type="text" class="form-control" name="last_name" id="last_name" value="{{  Session::has('input') ? $input['last_name'] : '' }}" placeholder="eg: Bond">
                </div>
                <div class="col form-group">
                  <label for="hired_from">Hired From</label>
                  <input type="date" class="form-control" name="hired_from" id="hired_from" value="{{  Session::has('input') ? $input['hired_from'] : '' }}">
                </div>
                <div class="col form-group">
                  <label for="hired_to">Hired To</label>
                  <input type="date" class="form-control" name="hired_to" id="hiredto" value="{{  Session::has('input') ? $input['hired_to'] : '' }}">
                </div>
              </div>

              <div class="form-row">
                <div class="col form-group">
                  <label for="email">Email</label>
                  <input type="text" class="form-control" id="email" name="email" placeholder="eg: dumspirpero@gmail.com" value="{{  Session::has('input') ? $input['email'] : '' }}">
                </div>

                <div class="col form-group">
                  <label for="company">Company</label>
                  <input type="text" class="form-control" id="company" name="company" placeholder="eg: GR Tech" value="{{  Session::has('input') ? $input['company'] : '' }}">
                </div>
              </div>

              <input type="submit" value="Search" class="btn btn-outline-primary">
            </form>
          </div>
        </div>
        <hr>
      </div>
        <div class="col-md-12">
          @if ($message = Session::get('success'))
            <div class="alert alert-success alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <strong>{{ $message }}</strong>
            </div>
          @endif
          @if ($message = Session::get('warning'))
            <div class="alert alert-warning alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <strong>{{ $message }}</strong>
            </div>
          @endif
            <div class="card">
                <div class="card-header">User List</div>
                <table class="table">
                  <th>First Name</th>
                  <th>Last Name</th>
                  <th>Email</th>
                  <th>Company</th>
                  <th>Date Hired</th>
                  <tbody id="userLoad">
                    @foreach($users as $user)
                    <tr>
                      <td>{{$user->first_name}}</td>
                      <td>{{$user->last_name}}</td>
                      <td>{{$user->email}}</td>
                      <td>{{$user->company}}</td>
                      <td>{{$user->date_added}}</td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
